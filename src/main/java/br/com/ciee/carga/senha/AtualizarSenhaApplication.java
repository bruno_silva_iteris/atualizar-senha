package br.com.ciee.carga.senha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtualizarSenhaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AtualizarSenhaApplication.class, args);
    }
}
