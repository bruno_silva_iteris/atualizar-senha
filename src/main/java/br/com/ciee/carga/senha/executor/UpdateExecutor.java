package br.com.ciee.carga.senha.executor;

import br.com.ciee.carga.senha.model.User;
import br.com.ciee.carga.senha.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class UpdateExecutor {

    private static final Logger log = LoggerFactory.getLogger(UpdateExecutor.class);

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public UpdateExecutor(UserRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    public void processar() {
        List<User> users = repository.findAll();
        users.forEach(this::atualizarSenha);
    }

    private void atualizarSenha(User user) {
        try {
            repository.updatePassword(user.getId(), passwordEncoder.encode(user.getPassword()));
        } catch (Exception e) {
            log.error("Erro ao atualizar o usuario de ID igual a {}", user.getId());
            e.printStackTrace();
        }
    }

}
