package br.com.ciee.carga.senha.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "USUARIOS")
@Entity
public class User {

    @Id
    private Long id;

    @Column(name = "NOME", length = 300)
    private String name;

    @Column(name = "EMAIL", length = 100)
    private String email;

    @Column(name = "CODIGO", nullable = false, length = 100)
    private String code;

    @Column(name = "SENHA", nullable = false, length = 100)
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(id);
        sb.append(" - ");
        sb.append(email);
        sb.append(" - ");
        sb.append(password);
        return sb.toString();
    }
}
