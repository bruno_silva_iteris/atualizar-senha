package br.com.ciee.carga.senha.repository;

import br.com.ciee.carga.senha.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Modifying
    @Transactional
    @Query("update User set password = :password where id = :id")
    void updatePassword(@Param("id") Long id,
                        @Param("password") String password);

}
